<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $table = 'Share';
    
    /**
     * @ManyToOne ( target = 'Resource')
     * @JoinColumn (name = 'resource' , referencedColumnName ="id" )
     *
     */
    public function resource(){
    	return $this->belongsTo('App\Resource' , 'resource');
    }
    
    /**
     * @ManyToOne ( target = 'User')
     * @JoinColumn (name = 'shared_by' , referencedColumnName = 'id')
     */
    public function sharedBy(){
    	return $this->belongsTo('App\User' ,'shared_by');
    }
    
}

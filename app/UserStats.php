<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStats extends Model
{
    protected $table = 'UserStats';
    
    /**
     * @OneToOne ( target = 'User')
     * @JoinColumn (name = "user_id" , referencedColumnName = "id")
     */
    public function user(){
    	return $this->belongsTo('App\User' , 'user_id');
    }
}

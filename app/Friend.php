<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $table = 'Friend';
    
    /**
     * @ManyToOne (target = "User")
     * @JoinColumn (name ="user_id" referencedColumn ="id")
     */
    public function user(){
    	return $this->belongsTo('App\User' , 'user_id');
    }
    
    /**
     * @OneToOne (target ="User")
     * @JoinColumn (name ="friend_id", referencedColumnName ="id")
     * 
     */
    public function friend(){
    	return $this->belongsTo('App\User' , 'friend_id');
    }
}

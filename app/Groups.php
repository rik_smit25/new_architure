<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $table = 'Groups';
    
    /**
     * @ManyToOne (target = "Resource")
     * @JoinColumn (name = "image" , referencedColumnName ="id")
     */
    public function image(){
    	return $this->belongsTo('Resource' , 'image');
    }
    
    /**
     * @ManyToOne ( target = "User")
     * @JoinColumn (name ="host" , referencedColumnName ="id" )
     * 
     */
    public function host(){
    	return $this->belongsTo('User' , 'host');
    }
    
    
}

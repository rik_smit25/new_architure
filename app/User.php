<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'User';
    
    /**
     * @ManyToOne (target ='Country')
     * @JoinColumn (name ="country_code" , referencedColumnName = "code")
     */
    public function country(){
    	return $this->belongsTo('App\Country' , 'country_code');
    }
    
    /**
     * @ManyToOne ( target ='Resource')
     * @JoinColumn (name ="photo" , referencedColumnName ="id")
     */
    public function photo(){
    	return $this->belongsTo('App\Resource' , 'photo');
    }
    
}

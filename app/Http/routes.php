<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::post('/api/login' , 'AuthController@login_post');
Route::post('/api/logout' , 'AuthController@logout_post');

Route::post('/api/test', 'AuthController@test_post');
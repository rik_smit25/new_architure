<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
	
    public function login_post(Request $request){
    	$email = $request->input('email');
    	$username = $request->input('username');
    	$password = $request->input('password');
    	
    	$errors = array();
    	if(!$email && !$username){
    		$errors[] = array("parameter" => "email/username","message" => "email/username is required");
    	}    	
    	if(!$password){
    		$errors[] = array("parameter" => "password" , "message" => "password is required");
    	}    	
  		if(count($errors) > 0){
  			return Response::json($errors, config('constants.HTTP_STATUS_UNPROCESSABLE_ENTITY'));
  		}
    	
  		$loginIdentifier = $username? $username : $email;
  		$identifierField = $username? 'username' : 'email';
  		
  		Log::info("Login attempt by $loginIdentifier");
  		$user = User::where(array($identifierField => $loginIdentifier, 'password'=>hash("sha512", $password,"")))->first();
  		
  		if($user == null){
  			Log::info("Login failed. $identifierField : $loginIdentifier");
  			return Response::json(null,config('constants.HTTP_STATUS_UNAUTHORIZED'));
  		}
  		
  		Session::put(config('constants.SESSION_KEY_LOGGED_IN_USER_ID') , $user->id);
  		Session::put(config('constants.SESSION_KEY_LOGGED_IN_USER_EMAIL') , $user->email);
  		  		
  		return Response::json(array("id" => $user->id) , config('constants.HTTP_STATUS_OK'));
    }
    
    public function test_post(){
    	return Response::json(array("email" => Session::get('constants.SESSION_KEY_LOGGED_IN_USER_EMAIL')), 200);
    }
    
    public function logout_post(){    	
    	Log::info("User logging out.");
    	Session::forget(config('constants.SESSION_KEY_LOGGED_IN_USER_ID'));
    	Session::forget(config('constants.SESSION_KEY_LOGGED_IN_USER_EMAIL'));    	
    	return Response::json(null, config('constants.HTTP_STATUS_OK'));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    protected $table = 'Relationship';
    
    /**
     * @OneToOne ( target = "User")
     * @JoinColumn ( name ="user_id" referencedColumnName ="id")
     */
    public function user(){
    	return $this->belongsTo('User' , 'user_id');
    }
    
    /**
     * @OneToOne ( target ="User" )
     * @JoinColumn (name ="relation_with" referencedColumn ="id")
     */
    public function with(){
    	return $this->belongsTo('User' ,'relationship_with');
    }
}

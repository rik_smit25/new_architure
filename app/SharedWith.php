<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharedWith extends Model
{
    protected $table = 'SharedWith';
    
    /**
     * @ManyToOne ( target = 'Share')
     * @JoinColumn (name = "shared_id" referencedColumnName ="id")
     */
    public function share(){
    	return $this->belongsTo('Share' , 'shared_id');
    }
    
    /**
     * @OneToOne ( target = 'User')
     * @JoinColumn (name ='user_id' , referencedColumnName = "id")
     */
    public function user(){
    	return $this->belongsTo('User' , 'user_id');
    }
    
}

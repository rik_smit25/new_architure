<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'Event';
    
    /**
     * @ManyToOne( target = 'Resource')
     * @JoinColumn (name = "image" referenceColumnName ="id")
     * 
     */
    public function image(){
    	return $this->belongsTo('App\Resouce' ,'image');
    }
    
    /**
     * @ManyToOne (target = "User")
     * @JoinColumn (name = "host", referneceColumnName ="id" )
     * 
     */
    public function host(){
    	return $this->belongsTo('App\User' , 'host');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected  $table = 'Comment';
    
    /**
     * @ManyToOne (target = "User")
     * @JoinColumn (name = "userId", referencedColumn ="id")
     * 
     */
    public function user(){
    	return $this->belongsTo('App\User' ,'userId');
    }
    
    /**
     * @ManyToOne (target = "Status")
     * @JoinColumn (name ="statusId" , referenceColumn ="id")
     * 
     */    
    public function status(){
    	return $this->belongsTo('App\Status' ,'statusId');
    }
}

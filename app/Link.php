<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'Link';
    
    /**
     * @OneToOne (target = 'Status')
     * @JoinColumn (name = "status_id" referencedColumn ="id" nullable =true)
     * 
     */
    public function status(){
    	return $this->belongsTo('Status' , 'status_id');
    }
    
    
}

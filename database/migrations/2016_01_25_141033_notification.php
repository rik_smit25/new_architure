<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Notification' , function(Blueprint $table){
        	$table->increments('id');
        	$table->integer('toUserId')->unsigned();
        	$table->integer('fromUserId')->unsigned();
        	$table->integer('notificationTime');
        	$table->tinyInteger('type');
        	$table->mediumText('text')->nullable();
        	$table->integer('statudId')->unsigned()->nullable();
        	$table->integer('eventId')->unsigned()->nullable();
        	$table->integer('groupId')->unsigned()->nullable();
        	$table->integer('shareId')->unsigned()->nullable();
        	$table->tinyInteger('read')->default(0);
        	$table->tinyInteger('actionTaken')->default(0);
        	
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Notification');
    }
}

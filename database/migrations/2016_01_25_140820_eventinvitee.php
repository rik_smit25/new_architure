<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Eventinvitee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EventInvitee' , function(Blueprint $table){
        	$table->integer('event_id')->unsigned();
        	$table->integer('user_id')->unsigned();
        	$table->boolean('attendanceStatus')->default(0);
        	$table->primary(array('event_id' , 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('EventInvitee');
    }
}

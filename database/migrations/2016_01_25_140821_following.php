<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Following extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Following' , function(Blueprint $table){
        	$table->integer('follower_id')->unsigned();
        	$table->integer('followed_user_id')->unsigned();
        	$table->primary(array('follower_id' , 'followed_user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Following');
    }
}

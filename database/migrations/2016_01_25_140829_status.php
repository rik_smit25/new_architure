<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Status extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Status' , function(Blueprint $table){
        	$table->increments('id');
     		$table->integer('type');
     		$table->string('text', 1100)->nullable();
     		$table->dateTime('postedOn');
     		$table->tinyInteger('deleted');
     		$table->dateTime('deletedOn')->nullable();
     		$table->integer('resourceId')->unsigned()->nullable();
     		$table->integer('postedBy')->unsigned();
     		$table->integer('groupId')->unsigned()->nullable();
     		$table->integer('eventId')->unsigned()->nullable();
     		$table->foreign('resourceId')->references('id')->on('Resource');
     		$table->foreign('postedBy')->references('id')->on('User');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Status');
    }
}

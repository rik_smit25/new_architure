<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Relationship' , function(Blueprint $table){
        	$table->integer('user_id')->primary();
        	$table->integer('relationship_with')->unsigned()->nullable();
        	$table->tinyInteger('status');
        	$table->tinyInteger('accepted');
        	$table->tinyInteger('showOnProfile');
            $table->foreign('relationship_with')->references('id')->on('User');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Relationship');
    }
}

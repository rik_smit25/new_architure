<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Groups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Groups' , function(Blueprint $table){
        	$table->increments('id');
        	$table->string('name' , 255);
        	$table->mediumText('description')->nullable();
        	$table->integer('image')->unsigned()->nullable();
        	$table->integer('host')->unsigned();
        	$table->boolean('deleted')->default(false);
        	$table->foreign('image')->references('id')->on('Resource');
        	$table->foreign('host')->references('id')->on('User');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Groups');
    }
}

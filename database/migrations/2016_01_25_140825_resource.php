<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Resource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Resource' , function(Blueprint $table){
        	$table->increments('id');
        	$table->integer('type');
        	$table->mediumText('content');
        	$table->mediumText('videoThumbnail');
        	$table->dateTime('createdAt');
        	$table->tinyInteger('orphan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Resource');
    }
}

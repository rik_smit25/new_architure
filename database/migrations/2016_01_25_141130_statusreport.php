<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Statusreport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StatusReport', function(Blueprint $table){
        	$table->integer('status_id')->unsigned();
        	$table->integer('reported_by')->unsigned();
        	$table->primary(array('status_id' ,'reported_by'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('StatusReport');
    }
}

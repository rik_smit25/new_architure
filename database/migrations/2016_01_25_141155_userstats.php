<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Userstats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserStats' , function(Blueprint $table){
        	$table->integer('user_id')->unsigned();
        	$table->integer('friends');
        	$table->integer('statuses');
        	$table->integer('profileViews');
        	$table->integer('followers');
        	$table->primary('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('UserStats');
    }
}

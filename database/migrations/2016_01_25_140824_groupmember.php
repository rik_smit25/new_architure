<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Groupmember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GroupMember' , function(Blueprint $table){
        	$table->integer('group_id')->unsigned();
        	$table->integer('user_id')->unsigned();
        	$table->primary(array('group_id' , 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GroupMember');
    }
}

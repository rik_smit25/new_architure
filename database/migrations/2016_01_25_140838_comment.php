<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Comment' , function(Blueprint $table){
        	$table->increments('id');
        	$table->integer('userId')->unsigned();
        	$table->integer('statusId')->unsigned();
        	$table->string('text',1100);
        	$table->dateTime('commentedOn');
        	$table->boolean('deleted');
        	$table->dateTime('deletedOn');
        	$table->foreign('userId')->references('id')->on('User');
        	$table->foreign('statusId')->references('id')->on('Status');
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Comment');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Blocklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BlockList' , function(Blueprint $table){
        	$table->integer('user_id')->unsigned();
        	$table->integer('blocks_user_id')->unsigned();
        	$table->primary(array('user_id' , 'blocks_user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('BlockList');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Share extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Share' , function(Blueprint $table){
        	$table->increments('id');
        	$table->integer('timerSeconds');
        	$table->string('caption' , 100);
        	$table->integer('resource')->unsigned();
        	$table->integer('shared_by')->unsigned();
        	$table->dateTime('createdAt');
        	$table->foreign('resource')->references('id')->on('Resource');
        	$table->foreign('shared_by')->references('id')->on('User');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

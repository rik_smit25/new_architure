<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Likes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Likes' , function(Blueprint $table){
        	$table->integer('status_id')->unsigned();
        	$table->integer('user_id')->unsigned();
        	$table->primary(array('status_id' , 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Likes');
    }
}

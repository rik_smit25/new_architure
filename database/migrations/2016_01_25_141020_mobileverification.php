<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mobileverification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MobileVerification' , function(Blueprint $table){
        	$table->string('mobileNo');
        	$table->string('verificationCode');
        	$table->dateTime('lastSmsSentOn');
        	$table->primary('mobileNo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MobileVerification');
    }
}

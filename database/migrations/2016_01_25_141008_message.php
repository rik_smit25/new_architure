<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Message extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Message' , function (Blueprint $table){
        	$table->increments('id');
        	$table->integer('fromUser')->unsigned();
        	$table->integer('toUser')->unsigned()->nullable();
        	$table->integer('toGroup')->unsigned()->nullable();
        	$table->integer('toEvent')->unsigned()->nullable();
        	$table->dateTime('sentAt');
        	$table->mediumText('body');
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Message');
    }
}

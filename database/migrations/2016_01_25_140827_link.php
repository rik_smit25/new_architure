<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Link extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Link' , function(Blueprint $table){
        	$table->integer('status_id')->unsigned();
        	$table->primary('status_id');
        	$table->string('title' , 1000)->nullable();
        	$table->string('description' ,1000)->nullable();
        	$table->string('imageUrl' , 1000)->nullable();
        	$table->string('caption' , 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Link');
    }
}

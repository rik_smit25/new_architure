<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User' , function(Blueprint $table){
        	$table->increments('id');
        	$table->string('username', 255);
        	$table->string('email' , 255);
        	$table->string('password' , 255);
        	$table->string('firstName' ,255);
        	$table->string('lastName' , 255);
        	$table->string('gender',1)->nullable();
        	$table->date('dob')->nullable();
        	$table->string('occupation' , 255)->nullable();
        	$table->integer('photo')->unsigned()->nullable();
        	$table->string('mobile', 18)->nullable();
        	$table->mediumText('description')->nullable();
        	$table->tinyInteger('profileVisibility');
        	$table->tinyInteger('friendRequestAcceptanceMode');
        	$table->tinyInteger('allowFollowing');
        	$table->string('paypalEmail' , 255)->nullable();
        	$table->string('country_code' ,2)->nullable();
        	$table->integer('adViewed');
        	$table->integer('adClicked');
        	$table->tinyInteger('walkthroughDone');
        	$table->dateTime('createdAt');
        	$table->foreign('country_code')->references('code')->on('Country');
        	$table->foreign('photo')->references('id')->on('Resource');        	
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('User');
    }
}

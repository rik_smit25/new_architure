<?php

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Userpayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserPayments' , function(Blueprint $table){
        	$table->integer('user_id')->unsigned();
        	$table->integer('year');
        	$table->integer('month');
        	$table->integer('adViewed');
        	$table->integer('adClicked');
        	$table->decimal('paymentAmount' ,10 , 2);
        	$table->dateTime('paymentDateTime');
        	$table->string('paymentCorrelationId',25);
        	$table->primary(array('user_id' , 'year' ,'month'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('UserPayments');
    }
}

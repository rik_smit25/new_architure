<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Friend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Friend' , function(Blueprint $table){
        	$table->integer('user_id')->unsigned();
        	$table->integer('friend_id')->unsigned();
        	$table->boolean('accepted');
        	$table->primary(array('user_id' , 'friend_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Friend');
    }
}
